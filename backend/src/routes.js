const { Router } = require('express')

const OngCreateValidator = require('./validators/OngCreateValidator'),
      IncidentListValidator = require('./validators/IncidentListValidator'),
      IncidentCreateValidator = require('./validators/IncidentCreateValidator'),
      IncidentDeleteValidator = require('./validators/IncidentDeleteValidator'),
      ProfileListValidator = require('./validators/ProfileListValidator')

const OngController = require('./controllers/OngController.js'),
      IncidentController = require('./controllers/IncidentController')
      ProfileController = require('./controllers/ProfileController')
      SessionController = require('./controllers/SessionController')

const routes = Router()

routes.post('/sessions', SessionController.create)

routes.get('/ongs', OngController.index)
routes.post('/ongs', OngCreateValidator(), OngController.create)

routes.get('/incidents', IncidentListValidator(), IncidentController.index)
routes.post('/incidents', IncidentCreateValidator(), IncidentController.create)
routes.delete('/incidents/:id', IncidentDeleteValidator(), IncidentController.delete)

routes.get('/profile', ProfileListValidator(), ProfileController.index)

module.exports = routes