const express = require('express'),
      cors = require('cors'),
      { errors } = require('celebrate')
      routes = require('./routes')

const app = express()

app.use(cors())
app.use(express.json()) // Transforma o JSON em objeto JavaScript
app.use(routes)
app.use(errors())

app.listen(3333)

module.exports = app