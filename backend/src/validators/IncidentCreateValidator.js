const { celebrate, Segments, Joi } = require('celebrate')

module.exports = function IncidentCreateValidator() {
   return (
      celebrate({
         [Segments.HEADERS]: Joi.object({
            authorization: Joi.string().required()
         })
         .unknown()
      }),
      celebrate({
         [Segments.BODY]: Joi.object().keys({
            title:       Joi.string().required(),
            description: Joi.string().required(),
            value:       Joi.number().required().min(1)
         })
      })
   )
}