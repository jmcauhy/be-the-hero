const { celebrate, Segments, Joi } = require('celebrate')

module.exports = function OngCreateValidator() {
   return (
      celebrate({
         [Segments.QUERY]: Joi.object().keys({
            page: Joi.number()
         })
      })
   )
}