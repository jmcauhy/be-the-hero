const { celebrate, Segments, Joi } = require('celebrate')

module.exports = function IncidentDeleteValidator() {
   return (
      celebrate({
         [Segments.PARAMS]: Joi.object().keys({
            id: Joi.number().required()
         })
      })
   )
}