<div align="center">
    <img alt="Be The Hero" src="./mobile/src/assets/logo@2x.png" />
</div>
<br/>
<div align="center">A project developed during the 11th Omnistack week, by [Rocketseat](https://github.com/rocketseat/)</div>
<br/>
<hr/>
<br/>
<div align="center">
    <img alt="Be The Hero" src="https://i.imgur.com/iTJeuct.png" />
</div>

## :clipboard: Description

Be The Hero was developed to connect people who wants to contribute to NGOs (Non-Governmental Organizations) that need help in specific cases.

## :computer: Technologies
This project was developed using [React](https://reactjs.org/), 
[React Native](https://facebook.github.io/react-native/) (with [Expo](https://expo.io/)), 
[Node.js](https://nodejs.org/en/) and [SQLite](https://www.sqlite.org/) (with [Express](https://expressjs.com/pt-br/) and [Knex.js](http://knexjs.org/)).

## :bulb: How to contribute

- Fork this repository;
- Create a branch with your feature: `git checkout -b my-feature`;
- Commit your changes: `git commit -m 'My new feature'`;
- Push to your branch: `git push origin my-feature`.

## :scroll: License

This project is under the [MIT](https://choosealicense.com/licenses/mit/) license.